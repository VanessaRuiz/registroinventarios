﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegistroInventarios.Clases
{
    internal class Empleado
    {
        public string Nombre { get; set; }
        public Int64 numIdentificacion { get; set; }
        public int Edad { get; set; }
        public int tiempoEmpresa { get; set; }
        public string Jornada { get; set; }
        public string comprasCompensar { get; set; }
        public string centrosRecreacionales { get; set; }

        public void CapturarDatos(string Nombre, Int64 numIdentificacion, int Edad, int tiempoEmpresa, string Jornada, string comprasCompensar, string centrosRecreacionales)
        {
            this.Nombre = Nombre;
            this.numIdentificacion = numIdentificacion;
            this.Edad = Edad;
            this.tiempoEmpresa = tiempoEmpresa;
            this.Jornada = Jornada;
            this.comprasCompensar = comprasCompensar;
            this.centrosRecreacionales = centrosRecreacionales;
        }
    }
}
