﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegistroInventarios.Clases
{

    internal class Inventario
    {
        public string NombreProductos { get; set; }
        public string TipoProducto { get; set; }
        public int NumeroUnidades { get; set; }
        public double ValorUnitario { get; set; }
        public double ValorIva { get; set; }
        public double ValorTotal { get; set; }

        public void LlenarDatos(string NombreProductos, string TipoProducto, int NumeroUnidades, double ValorUnitario, double ValorIva, double ValorTotal)
        {

            this.NombreProductos = NombreProductos;
            this.TipoProducto = TipoProducto;
            this.NumeroUnidades = NumeroUnidades;
            this.ValorUnitario = ValorUnitario;
            this.ValorIva = ValorIva;
            this.ValorTotal = ValorTotal;
            

        }

    }
}
