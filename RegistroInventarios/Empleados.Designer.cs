﻿
namespace RegistroInventarios
{
    partial class Empleados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Empleados));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dataInformacion = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.lblIdentificacion = new System.Windows.Forms.Label();
            this.lblEdad = new System.Windows.Forms.Label();
            this.rbDiurno = new System.Windows.Forms.RadioButton();
            this.rbNocturno = new System.Windows.Forms.RadioButton();
            this.nudTiempo = new System.Windows.Forms.NumericUpDown();
            this.lblTiEmpresa = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtNIdentificacion = new System.Windows.Forms.TextBox();
            this.txtEdad = new System.Windows.Forms.TextBox();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnEditar = new System.Windows.Forms.Button();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.nudEmpleadosRegis = new System.Windows.Forms.NumericUpDown();
            this.btnGrafica = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataInformacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTiempo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEmpleadosRegis)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(1, 1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(734, 100);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::RegistroInventarios.Properties.Resources.img_volver;
            this.pictureBox1.Location = new System.Drawing.Point(6, 11);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(68, 39);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(269, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(150, 20);
            this.label4.TabIndex = 1;
            this.label4.Text = "Giovanny Orozco ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(134, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(480, 26);
            this.label3.TabIndex = 0;
            this.label3.Text = "Bienvenidos a UCompensar - Modulo Empleados";
            // 
            // dataInformacion
            // 
            this.dataInformacion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataInformacion.Location = new System.Drawing.Point(10, 236);
            this.dataInformacion.Name = "dataInformacion";
            this.dataInformacion.RowHeadersWidth = 51;
            this.dataInformacion.Size = new System.Drawing.Size(450, 166);
            this.dataInformacion.TabIndex = 6;
            this.dataInformacion.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataInformacion_CellDoubleClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(129, 206);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(217, 16);
            this.label1.TabIndex = 7;
            this.label1.Text = "Informacion de los empleados";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(522, 236);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 16);
            this.label2.TabIndex = 8;
            this.label2.Text = "Editar empleados";
            this.label2.Visible = false;
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(466, 266);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(47, 13);
            this.lblNombre.TabIndex = 9;
            this.lblNombre.Text = "Nombre:";
            this.lblNombre.Visible = false;
            // 
            // lblIdentificacion
            // 
            this.lblIdentificacion.AutoSize = true;
            this.lblIdentificacion.Location = new System.Drawing.Point(466, 304);
            this.lblIdentificacion.Name = "lblIdentificacion";
            this.lblIdentificacion.Size = new System.Drawing.Size(85, 13);
            this.lblIdentificacion.TabIndex = 10;
            this.lblIdentificacion.Text = "N° Identificacion";
            this.lblIdentificacion.Visible = false;
            // 
            // lblEdad
            // 
            this.lblEdad.AutoSize = true;
            this.lblEdad.Location = new System.Drawing.Point(466, 340);
            this.lblEdad.Name = "lblEdad";
            this.lblEdad.Size = new System.Drawing.Size(32, 13);
            this.lblEdad.TabIndex = 11;
            this.lblEdad.Text = "Edad";
            this.lblEdad.Visible = false;
            // 
            // rbDiurno
            // 
            this.rbDiurno.AutoSize = true;
            this.rbDiurno.Location = new System.Drawing.Point(497, 414);
            this.rbDiurno.Name = "rbDiurno";
            this.rbDiurno.Size = new System.Drawing.Size(56, 17);
            this.rbDiurno.TabIndex = 12;
            this.rbDiurno.TabStop = true;
            this.rbDiurno.Text = "Diurno";
            this.rbDiurno.UseVisualStyleBackColor = true;
            this.rbDiurno.Visible = false;
            // 
            // rbNocturno
            // 
            this.rbNocturno.AutoSize = true;
            this.rbNocturno.Location = new System.Drawing.Point(592, 414);
            this.rbNocturno.Name = "rbNocturno";
            this.rbNocturno.Size = new System.Drawing.Size(69, 17);
            this.rbNocturno.TabIndex = 13;
            this.rbNocturno.TabStop = true;
            this.rbNocturno.Text = "Nocturno";
            this.rbNocturno.UseVisualStyleBackColor = true;
            this.rbNocturno.Visible = false;
            // 
            // nudTiempo
            // 
            this.nudTiempo.Location = new System.Drawing.Point(592, 377);
            this.nudTiempo.Name = "nudTiempo";
            this.nudTiempo.Size = new System.Drawing.Size(120, 20);
            this.nudTiempo.TabIndex = 14;
            this.nudTiempo.Visible = false;
            // 
            // lblTiEmpresa
            // 
            this.lblTiEmpresa.AutoSize = true;
            this.lblTiEmpresa.Location = new System.Drawing.Point(466, 377);
            this.lblTiEmpresa.Name = "lblTiEmpresa";
            this.lblTiEmpresa.Size = new System.Drawing.Size(86, 13);
            this.lblTiEmpresa.TabIndex = 15;
            this.lblTiEmpresa.Text = "Tiempo Empresa";
            this.lblTiEmpresa.Visible = false;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(592, 266);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(121, 20);
            this.txtNombre.TabIndex = 16;
            this.txtNombre.Visible = false;
            // 
            // txtNIdentificacion
            // 
            this.txtNIdentificacion.Location = new System.Drawing.Point(592, 304);
            this.txtNIdentificacion.Name = "txtNIdentificacion";
            this.txtNIdentificacion.Size = new System.Drawing.Size(121, 20);
            this.txtNIdentificacion.TabIndex = 17;
            this.txtNIdentificacion.Visible = false;
            // 
            // txtEdad
            // 
            this.txtEdad.Location = new System.Drawing.Point(592, 340);
            this.txtEdad.Name = "txtEdad";
            this.txtEdad.Size = new System.Drawing.Size(121, 20);
            this.txtEdad.TabIndex = 18;
            this.txtEdad.Visible = false;
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(385, 422);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(75, 23);
            this.btnEliminar.TabIndex = 19;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Visible = false;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.Location = new System.Drawing.Point(637, 459);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(75, 23);
            this.btnEditar.TabIndex = 20;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = true;
            this.btnEditar.Visible = false;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.Location = new System.Drawing.Point(637, 117);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(75, 23);
            this.btnRegistrar.TabIndex = 21;
            this.btnRegistrar.Text = "Registrar";
            this.btnRegistrar.UseVisualStyleBackColor = true;
            this.btnRegistrar.Click += new System.EventHandler(this.btnRegistrar_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(122, 117);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(317, 16);
            this.label5.TabIndex = 22;
            this.label5.Text = "Ingrese el numero de empleados a Registrar";
            // 
            // nudEmpleadosRegis
            // 
            this.nudEmpleadosRegis.Location = new System.Drawing.Point(447, 117);
            this.nudEmpleadosRegis.Name = "nudEmpleadosRegis";
            this.nudEmpleadosRegis.Size = new System.Drawing.Size(63, 20);
            this.nudEmpleadosRegis.TabIndex = 23;
            // 
            // btnGrafica
            // 
            this.btnGrafica.Location = new System.Drawing.Point(10, 422);
            this.btnGrafica.Name = "btnGrafica";
            this.btnGrafica.Size = new System.Drawing.Size(92, 23);
            this.btnGrafica.TabIndex = 24;
            this.btnGrafica.Text = "Mostrar Grafica";
            this.btnGrafica.UseVisualStyleBackColor = true;
            this.btnGrafica.Visible = false;
            this.btnGrafica.Click += new System.EventHandler(this.btnGrafica_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(418, 285);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 16);
            this.label6.TabIndex = 25;
            this.label6.Text = "Id";
            // 
            // Empleados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(736, 495);
            this.Controls.Add(this.btnGrafica);
            this.Controls.Add(this.nudEmpleadosRegis);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnRegistrar);
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.txtEdad);
            this.Controls.Add(this.txtNIdentificacion);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.lblTiEmpresa);
            this.Controls.Add(this.nudTiempo);
            this.Controls.Add(this.rbNocturno);
            this.Controls.Add(this.rbDiurno);
            this.Controls.Add(this.lblEdad);
            this.Controls.Add(this.lblIdentificacion);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataInformacion);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label6);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Empleados";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Empleados";
            this.Load += new System.EventHandler(this.Empleados_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataInformacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTiempo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEmpleadosRegis)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridView dataInformacion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label lblIdentificacion;
        private System.Windows.Forms.Label lblEdad;
        private System.Windows.Forms.RadioButton rbDiurno;
        private System.Windows.Forms.RadioButton rbNocturno;
        private System.Windows.Forms.NumericUpDown nudTiempo;
        private System.Windows.Forms.Label lblTiEmpresa;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtNIdentificacion;
        private System.Windows.Forms.TextBox txtEdad;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnEditar;
        private System.Windows.Forms.Button btnRegistrar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown nudEmpleadosRegis;
        private System.Windows.Forms.Button btnGrafica;
        private System.Windows.Forms.Label label6;
    }
}