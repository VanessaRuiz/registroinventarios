﻿using RegistroInventarios.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace RegistroInventarios
{
    public partial class Empleados : Form
    {
        public Empleados()
        {
            InitializeComponent();
        }

        string Nombre = "";
        Int64 numIdentificacion = 0;
        int Edad = 0;
        int tiempoEmpresa = 0;
        string Jornada = "";
        string comprasCompensar = "";
        string centrosRecreacionales = "";

        string[] nombres;
        string[,] datosCompletos;
        Empleado empleado = new Empleado();

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Menu formMenu = new Menu();
            formMenu.Show();
            this.Hide();
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (nudEmpleadosRegis.Value != 0)
            {
                int numEmpleados = Convert.ToInt16(nudEmpleadosRegis.Value);
                nombres = new string[numEmpleados];
                datosCompletos = new string[numEmpleados, 6];

                for (int i = 0; i < nombres.Length; i++)
                {
                    int cont = i + 1;
                    Nombre = Interaction.InputBox("Ingrese el nombre del empleado N° " + cont);
                    numIdentificacion = Convert.ToInt64(Interaction.InputBox("Ingrese el numero de identificacion del empleado N° " + cont));
                    Edad = Convert.ToInt32(Interaction.InputBox("Ingrese la edad del empleado N° " + cont));
                    tiempoEmpresa = Convert.ToInt32(Interaction.InputBox("Ingrese el tiempo en la empresa (Años) del empleado N° " + cont));
                    Jornada = Interaction.InputBox("Ingrese la jornada del empleado N° " + cont);

                    if (tiempoEmpresa == -1)
                    {
                        comprasCompensar = "Descuento 15%";
                        centrosRecreacionales = "Descuento 20%";
                    }
                    else if (tiempoEmpresa >= 1 && tiempoEmpresa <= 5)
                    {
                        comprasCompensar = "Descuento 30%";
                        centrosRecreacionales = "Descuento 30%";
                    }
                    else if (tiempoEmpresa >= 6)
                    {
                        comprasCompensar = "Descuento 50%";
                        centrosRecreacionales = "Descuento 60%";
                    }

                    empleado.CapturarDatos(Nombre, numIdentificacion, Edad, tiempoEmpresa, Jornada, comprasCompensar, centrosRecreacionales);

                    llenarArrays(i);
                }

                nudEmpleadosRegis.Value = 0;
                mostrarDatos();
                MostrarCampos();
            }
            else
                MessageBox.Show("Debe seleccionar cuantos empleados se registraran");
        }

        void llenarArrays(int i)
        {
            nombres[i] = empleado.Nombre;
            datosCompletos[i, 0] = Convert.ToString(empleado.numIdentificacion);
            datosCompletos[i, 1] = Convert.ToString(empleado.Edad);
            datosCompletos[i, 2] = Convert.ToString(empleado.tiempoEmpresa);
            datosCompletos[i, 3] = Convert.ToString(empleado.Jornada);
            datosCompletos[i, 4] = Convert.ToString(empleado.comprasCompensar);
            datosCompletos[i, 5] = Convert.ToString(empleado.centrosRecreacionales);
        }

        void mostrarDatos()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Id");
            dt.Columns.Add("Nombre");
            dt.Columns.Add("Num Identificacion");
            dt.Columns.Add("Edad");
            dt.Columns.Add("Tiempo Empresa");
            dt.Columns.Add("Jornada");
            dt.Columns.Add("Descuento compras compensar");
            dt.Columns.Add("Descuento centros recreativos");


            for (int i = 0; i < nombres.Length; i++)
            {
                empleado.Nombre = nombres[i];
                empleado.numIdentificacion = Convert.ToInt64(datosCompletos[i, 0]);
                empleado.Edad = Convert.ToInt32(datosCompletos[i, 1]);
                empleado.tiempoEmpresa = Convert.ToInt16(datosCompletos[i, 2]);
                empleado.Jornada = datosCompletos[i, 3];
                empleado.comprasCompensar = datosCompletos[i, 4];
                empleado.centrosRecreacionales = datosCompletos[i, 5];

                if (empleado.Nombre != "" && empleado.numIdentificacion != 0 && empleado.Edad != 0)
                {

                    dt.Rows.Add(
                        i,
                        empleado.Nombre,
                        empleado.numIdentificacion,
                        empleado.Edad,
                        empleado.tiempoEmpresa,
                        empleado.Jornada,
                        empleado.comprasCompensar,
                        empleado.centrosRecreacionales
                        );
                }
            }

            dataInformacion.DataSource = dt;
        }

        private void btnGrafica_Click(object sender, EventArgs e)
        {
            GraficaEmpleados formGrafica = new GraficaEmpleados();
            formGrafica.datosGraficar(nombres, datosCompletos);
            formGrafica.Show();
            this.Hide();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (label6.Text != "Id")
            {
                int i = int.Parse(label6.Text);

                nombres[i] = "";
                datosCompletos[i, 0] = Convert.ToString(0);
                datosCompletos[i, 1] = Convert.ToString(0);
                datosCompletos[i, 2] = Convert.ToString(0);
                datosCompletos[i, 3] = "";
                datosCompletos[i, 4] = "";
                datosCompletos[i, 5] = "";

                mostrarDatos();

                limpiarCampos();
            }
            else
                MessageBox.Show("Dar doble click sobre el empleado a eliminar");
        }

        void MostrarCampos()
        {
            label2.Visible = true;
            lblNombre.Visible = true;
            lblIdentificacion.Visible = true;
            lblEdad.Visible = true;
            lblTiEmpresa.Visible = true;
            txtNombre.Visible = true;
            txtEdad.Visible = true;
            txtNIdentificacion.Visible = true;
            nudTiempo.Visible = true;
            rbDiurno.Visible = true;
            rbNocturno.Visible = true;
            btnEditar.Visible = true;
            btnEliminar.Visible = true;
            btnGrafica.Visible = true;
        }

        void limpiarCampos()
        {
            label6.Text = "Id";
            txtNombre.Text = "";
            txtEdad.Text = "";
            txtNIdentificacion.Text = "";
            nudTiempo.Value = 0;
            rbDiurno.Checked = false;
            rbNocturno.Checked = false;
        }

        private void dataInformacion_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            label6.Text = dataInformacion.CurrentRow.Cells[0].Value.ToString();
            txtNombre.Text = dataInformacion.CurrentRow.Cells[1].Value.ToString();
            txtNIdentificacion.Text = dataInformacion.CurrentRow.Cells[2].Value.ToString();
            txtEdad.Text = dataInformacion.CurrentRow.Cells[3].Value.ToString();
            nudTiempo.Value = decimal.Parse( dataInformacion.CurrentRow.Cells[4].Value.ToString());
            string jornada = dataInformacion.CurrentRow.Cells[5].Value.ToString();

            if (jornada == "Diurna" || jornada == "diurna")
            {
                rbDiurno.Checked = true;
            }
            else if (jornada == "Nocturna" || jornada == "nocturna")
            {
                rbNocturno.Checked = true;
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (label6.Text != "Id")
            {
                int i = int.Parse(label6.Text);

                nombres[i] = txtNombre.Text;
                datosCompletos[i, 0] = txtNIdentificacion.Text;
                datosCompletos[i, 1] = txtNIdentificacion.Text;
                datosCompletos[i, 2] = Convert.ToString(nudTiempo.Value);

                if (rbDiurno.Checked == true)
                {
                    datosCompletos[i, 3] = "Diurna";
                }
                else if (rbNocturno.Checked == true)
                {
                    datosCompletos[i, 3] = "Nocturna";
                }

                if (nudTiempo.Value == -1)
                {
                    datosCompletos[i, 4] = "Descuento 15%";
                    datosCompletos[i, 5] = "Descuento 20%";
                }
                else if (nudTiempo.Value >= 1 && nudTiempo.Value <= 5)
                {
                    datosCompletos[i, 4] = "Descuento 30%";
                    datosCompletos[i, 5] = "Descuento 30%";
                }
                else if (nudTiempo.Value >= 6)
                {
                    datosCompletos[i, 4] = "Descuento 50%";
                    datosCompletos[i, 5] = "Descuento 60%";
                }
            }
            else
                MessageBox.Show("Debe dar doble click sobre el empleado a editar");

            limpiarCampos();

            mostrarDatos();
        }

        private void Empleados_Load(object sender, EventArgs e)
        {
            if (nombres != null)
            {
                mostrarDatos();
                MostrarCampos();
            }
        }

        public void LlenarArrays(string[] nombres, string[,] datosCompletos)
        {
            this.nombres = nombres;
            this.datosCompletos = datosCompletos;
        }
    }
}
