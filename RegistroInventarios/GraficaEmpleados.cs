﻿using RegistroInventarios.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegistroInventarios
{
    public partial class GraficaEmpleados : Form
    {
        public GraficaEmpleados()
        {
            InitializeComponent();
        }

        string[] nombres;
        string[,] jornadas;
        int Diurna = 0;
        int Nocturna = 0;

        public void datosGraficar(string[] nombre, string[,] jornada)
        {
            nombres = nombre;
            jornadas = jornada;
        }

        private void GraficaEmpleados_Paint(object sender, PaintEventArgs e)
        {
            realizarGrafica();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Empleados formEmpleados = new Empleados();
            formEmpleados.LlenarArrays(nombres, jornadas);
            formEmpleados.Show();
            this.Hide();
        }

        void realizarGrafica()
        {
            Graphics lienzo = CreateGraphics();
            int total = Diurna + Nocturna;
            int grados1 = (Diurna * 360) / total;
            int grados2 = (Nocturna * 360) / total;
            lienzo.FillPie(new SolidBrush(Color.Red), 200, 110, 300, 300, 0, grados1);
            lienzo.FillPie(new SolidBrush(Color.Blue), 200, 110, 300, 300, grados1, grados2);

            lienzo.FillRectangle(new SolidBrush(Color.Red), 500, 350, 20, 20);
            lienzo.DrawString("Jornada Diurna " + Diurna.ToString(), new Font("Arial", 15), new SolidBrush(Color.Red), 520, 350);
            lienzo.FillRectangle(new SolidBrush(Color.Blue), 500, 380, 20, 20);
            lienzo.DrawString("Jornada Nocturna " + Nocturna.ToString(), new Font("Arial", 15), new SolidBrush(Color.Blue), 520, 380);
        }

        private void GraficaEmpleados_Load(object sender, EventArgs e)
        {
            Empleado empleado = new Empleado();

            for (int i = 0; i < nombres.Length; i++)
            {
                empleado.Jornada = jornadas[i, 3];

                if (empleado.Jornada == "Nocturna" || empleado.Jornada == "nocturna")
                {
                    Nocturna++;
                }
                else if (empleado.Jornada == "Diurna" || empleado.Jornada == "diurna")
                {
                    Diurna++;
                }
            }
        }
    }
}
