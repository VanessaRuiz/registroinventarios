﻿using RegistroInventarios.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegistroInventarios
{
    public partial class GraficaInventarios : Form
    {
        public GraficaInventarios()
        {
            InitializeComponent();
        }

        string[] nombres;
        string[,] tipoProducto;
        int Aseo = 0;
        int Papeleria = 0;
        int Viveres = 0;
        int Mascotas = 0;
        int Otros = 0;

        public void datosGraficar(string[] nombre, string[,] tipoProductos)
        {
            nombres = nombre;
            tipoProducto = tipoProductos;
        }

        private void GraficaInventarios_Paint(object sender, PaintEventArgs e)
        {
            realizarGrafica();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Inventarios formInventarios = new Inventarios();
            formInventarios.LlenarArrays(nombres,tipoProducto);
            formInventarios.Show();
            this.Hide();
        }

        void realizarGrafica()
        {
            Graphics lienzo = CreateGraphics();
            int total = Aseo + Papeleria + Viveres + Mascotas + Otros;
            int grados1 = (Aseo * 360) / total;
            int grados2 = (Papeleria * 360) / total;
            int grados3 = (Viveres * 360) / total;
            int grados4 = (Mascotas * 360) / total;
            int grados5 = (Otros * 360) / total;
            lienzo.FillPie(new SolidBrush(Color.Red), 200, 110, 300, 300, 0, grados1);
            lienzo.FillPie(new SolidBrush(Color.Blue), 200, 110, 300, 300, grados1, grados2);
            lienzo.FillPie(new SolidBrush(Color.Orange), 200, 110, 300, 300, grados1 + grados2, grados3);
            lienzo.FillPie(new SolidBrush(Color.Green), 200, 110, 300, 300, grados1 + grados2 + grados3, grados4);
            lienzo.FillPie(new SolidBrush(Color.Pink), 200, 110, 300, 300, grados1 + grados2 + grados3 + grados4, grados5);

            lienzo.FillRectangle(new SolidBrush(Color.Red), 520, 250, 20, 20);
            lienzo.DrawString("Aseo " + Aseo.ToString(), new Font("Arial", 15), new SolidBrush(Color.Red), 540, 250);
            lienzo.FillRectangle(new SolidBrush(Color.Blue), 520, 280, 20, 20);
            lienzo.DrawString("papeleria " + Papeleria.ToString(), new Font("Arial", 15), new SolidBrush(Color.Blue), 540, 280);
            lienzo.FillRectangle(new SolidBrush(Color.Orange), 520, 310, 20, 20);
            lienzo.DrawString("Viverers " + Viveres.ToString(), new Font("Arial", 15), new SolidBrush(Color.Orange), 540, 310);
            lienzo.FillRectangle(new SolidBrush(Color.Green), 520, 350, 20, 20);
            lienzo.DrawString("Mascotas " + Mascotas.ToString(), new Font("Arial", 15), new SolidBrush(Color.Green), 540, 350);
            lienzo.FillRectangle(new SolidBrush(Color.Pink), 520, 380, 20, 20);
            lienzo.DrawString("Otros " + Otros.ToString(), new Font("Arial", 15), new SolidBrush(Color.Pink), 540, 380);
        }

        private void GraficaInventarios_Load(object sender, EventArgs e)
        {
            Inventario inventario = new Inventario();

            for (int i = 0; i < nombres.Length; i++)
            {
                inventario.TipoProducto = tipoProducto[i, 0];

                if (inventario.TipoProducto == "Aseo" || inventario.TipoProducto == "aseo")
                {
                    Aseo++;
                }
                else if (inventario.TipoProducto == "Papeleria" || inventario.TipoProducto == "papeleria")
                {
                    Papeleria++;
                }
                else if (inventario.TipoProducto == "Viveres" || inventario.TipoProducto == "viveres")
                {
                    Viveres++;
                }
                else if (inventario.TipoProducto == "Mascotas" || inventario.TipoProducto == "mascotas")
                {
                    Mascotas++;
                }
                else if (inventario.TipoProducto == "Otros" || inventario.TipoProducto == "otros")
                {
                    Otros++;
                }
            }
        }
    }
}
