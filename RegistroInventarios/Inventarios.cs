﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RegistroInventarios.Clases;


namespace RegistroInventarios
{
    public partial class Inventarios : Form
    {
        public Inventarios()
        {
            InitializeComponent();
        }

        string NombreProducto = "";
        string TipoProducto = "";
        int NumeroUnidades = 0;
        double ValorUnitario = 0;
        double ValorIva = 0;
        double ValorTotal = 0;


        string[] nombres;
        string[,] datosCompletos;

        Inventario inventario = new Inventario();


        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Menu formMenu = new Menu();
            formMenu.Show();
            this.Hide();
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (nudInventariosRegis.Value != 0)
            {
                int numEmpleados = Convert.ToInt16(nudInventariosRegis.Value);
                nombres = new string[numEmpleados];
                datosCompletos = new string[numEmpleados, 6];

                for (int i = 0; i < nombres.Length; i++)
                {
                    int cont = i + 1;
                    NombreProducto = Interaction.InputBox("Ingrese el nombre del producto " + cont);
                    TipoProducto = Interaction.InputBox("Ingrese el tipo de producto " + cont);
                    NumeroUnidades = Convert.ToInt32(Interaction.InputBox("Ingrese el numero de unidades " + cont));
                    ValorUnitario = Convert.ToDouble(Interaction.InputBox("Ingrese el valor unitario " + cont));

                    double totalUnidades = NumeroUnidades * ValorUnitario;

                    if (TipoProducto == "aseo")
                    {
                        ValorIva = (totalUnidades * 19) / 100;
                        ValorTotal = totalUnidades + ValorIva;
                    }
                    else if (TipoProducto == "papeleria")
                    {
                        ValorIva = (totalUnidades * 9) / 100;
                        ValorTotal = totalUnidades + ValorIva;
                    }
                    else if (TipoProducto == "viveres")
                    {
                        ValorIva = (totalUnidades * 15) / 100;
                        ValorTotal = totalUnidades + ValorIva;
                    }
                    else if (TipoProducto == "mascotas")
                    {
                        ValorIva = (totalUnidades * 16) / 100;
                        ValorTotal = totalUnidades + ValorIva;
                    }
                    else if (TipoProducto == "otros")
                    {
                        ValorIva = (totalUnidades * 10) / 100;
                        ValorTotal = totalUnidades + ValorIva;
                    }

                    inventario.LlenarDatos(NombreProducto, TipoProducto, NumeroUnidades, ValorUnitario, ValorIva, ValorTotal);

                    llenarArrys(i);
                }

                nudInventariosRegis.Value = 0;
                mostrarDatos();
                mostrarCampos();
            }
            else
                MessageBox.Show("Ingrese el numero de productos a registrar");

        }

        void llenarArrys(int i)
        {
            nombres[i] = inventario.NombreProductos;
            datosCompletos[i, 0] = Convert.ToString(inventario.TipoProducto);
            datosCompletos[i, 1] = Convert.ToString(inventario.NumeroUnidades);
            datosCompletos[i, 2] = Convert.ToString(inventario.ValorUnitario);
            datosCompletos[i, 3] = Convert.ToString(inventario.ValorIva);
            datosCompletos[i, 4] = Convert.ToString(inventario.ValorTotal);
        }

        void mostrarDatos()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Id");
            dt.Columns.Add("Nombre de Producto");
            dt.Columns.Add("Tipo de Producto");
            dt.Columns.Add("Numero de Unidades");
            dt.Columns.Add("Valor Unitario");
            dt.Columns.Add("Valor Iva");
            dt.Columns.Add("Valor total ");



            for (int i = 0; i < nombres.Length; i++)
            {
                inventario.NombreProductos = nombres[i];
                inventario.TipoProducto = datosCompletos[i, 0];
                inventario.NumeroUnidades = Convert.ToInt32(datosCompletos[i, 1]);
                inventario.ValorUnitario = Convert.ToDouble(datosCompletos[i, 2]);
                inventario.ValorIva = Convert.ToInt32(datosCompletos[i, 3]);
                inventario.ValorTotal = Convert.ToDouble(datosCompletos[i, 4]);

                if (inventario.NombreProductos != "" && inventario.TipoProducto  != "" && inventario.NumeroUnidades != 0)
                {
                    dt.Rows.Add(
                        i,
                        inventario.NombreProductos,
                        inventario.TipoProducto,
                        inventario.NumeroUnidades,
                        inventario.ValorUnitario,
                        inventario.ValorIva,
                        inventario.ValorTotal
                        );
                }
            }

            dataInformacion.DataSource = dt;
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (label1.Text != "Id")
            {

                int i = int.Parse(label1.Text);

                nombres[i] = "";
                datosCompletos[i, 0] = "";
                datosCompletos[i, 1] = Convert.ToString(0);
                datosCompletos[i, 2] = Convert.ToString(0);
                datosCompletos[i, 3] = Convert.ToString(0);
                datosCompletos[i, 4] = Convert.ToString(0);

                mostrarDatos();

                vaciarCampos();
            }
            else
                MessageBox.Show("Dar doble click en el registro a eliminar");
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (label1.Text != "Id")
            {

                int i = int.Parse(label1.Text);
                NumeroUnidades = Convert.ToInt32(nudUnidades.Value);
                ValorUnitario = double.Parse(txtValorUnitario.Text);
                TipoProducto = txtTipoProducto.Text;

                nombres[i] = txtNombreProducto.Text;
                datosCompletos[i, 0] = txtTipoProducto.Text;
                datosCompletos[i, 1] = nudUnidades.Value.ToString();
                datosCompletos[i, 2] = txtValorUnitario.Text;

                double totalUnidades = NumeroUnidades * ValorUnitario;

                if (TipoProducto == "aseo")
                {
                    ValorIva = (totalUnidades * 19) / 100;
                    ValorTotal = totalUnidades + ValorIva;
                }
                else if (TipoProducto == "papeleria")
                {
                    ValorIva = (totalUnidades * 9) / 100;
                    ValorTotal = totalUnidades + ValorIva;
                }
                else if (TipoProducto == "viveres")
                {
                    ValorIva = (totalUnidades * 15) / 100;
                    ValorTotal = totalUnidades + ValorIva;
                }
                else if (TipoProducto == "mascotas")
                {
                    ValorIva = (totalUnidades * 16) / 100;
                    ValorTotal = totalUnidades + ValorIva;
                }
                else if (TipoProducto == "otros")
                {
                    ValorIva = (totalUnidades * 10) / 100;
                    ValorTotal = totalUnidades + ValorIva;
                }

                datosCompletos[i, 3] = ValorIva.ToString();
                datosCompletos[i, 4] = ValorTotal.ToString();

                mostrarDatos();

                vaciarCampos();
            }
            else
                MessageBox.Show("Dar dobble click sobre el registro a editar");
        }

        private void dataInformacion_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            label1.Text = dataInformacion.CurrentRow.Cells[0].Value.ToString();
            txtNombreProducto.Text = dataInformacion.CurrentRow.Cells[1].Value.ToString();
            txtTipoProducto.Text = dataInformacion.CurrentRow.Cells[2].Value.ToString();
            nudUnidades.Value = decimal.Parse(dataInformacion.CurrentRow.Cells[3].Value.ToString());
            txtValorUnitario.Text = dataInformacion.CurrentRow.Cells[4].Value.ToString();

        }

        void vaciarCampos()
        {
            label1.Text = "Id";
            txtNombreProducto.Text = "";
            txtTipoProducto.Text = "";
            nudUnidades.Value = 0;
            txtValorUnitario.Text = "";
        }

        void mostrarCampos()
        {
            nudInventariosRegis.Value = 0;
            label2.Visible = true;
            txtNombreProducto.Visible = true;
            txtTipoProducto.Visible = true;
            nudUnidades.Visible = true;
            txtValorUnitario.Visible = true;
            lblNombreProducto.Visible = true;
            lblTipoProducto.Visible = true;
            lblValorUnitario.Visible = true;
            lblUnidades.Visible = true;
            btnEditar.Visible = true;
            btnEliminar.Visible = true;
            btnGrafica.Visible = true;
        }

        private void btnGrafica_Click(object sender, EventArgs e)
        {
            GraficaInventarios formGrafica = new GraficaInventarios();
            formGrafica.datosGraficar(nombres, datosCompletos);
            formGrafica.Show();
            this.Hide();
        }

        private void Inventarios_Load(object sender, EventArgs e)
        {
            if (nombres != null)
            {
                mostrarDatos();
                mostrarCampos();
            }
        }

        public void LlenarArrays(string[] nombres, string[,] datosCompletos)
        {
            this.nombres = nombres;
            this.datosCompletos = datosCompletos;
        }
    }

}
